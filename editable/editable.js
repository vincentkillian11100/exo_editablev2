let key_Nm = document.getElementById("key_name");
let dataPrint = document.getElementById("dataPrint");
let area = document.getElementById("area");
let requestPerson = "../data/person.json";
let requestPoints = "../data/points.json";
let requestProducts = "../data/product.json";
let val_text = document.getElementById("input_search")
let monTab = [];

let person_json = document.getElementById("person");
person_json.addEventListener("click", function (e) {
  fetch(requestPerson) // j'envoie ma requête
    .then(
      // quand je reçois la réponse
      (response) => response.json() // je récupère le JSON de la réponse
    )
    .then(
      // quand c'est pret
      (tab) => {
        monTab = tab;
        creatTabperson(monTab);
        // je mets mes data dans ma variable
      }
    )
    .catch((err) => {
      console.log(err); // j'affiche les erreurs HTTP au cas où
    });
  area.innerHTML = "";
  key_Nm.innerHTML = "";
  area.style.gridTemplateColumns = "repeat(7, 1fr)";
  key_Nm.style.gridTemplateColumns = "repeat(7, 1fr)";
});

let points_json = document.getElementById("points");
points_json.addEventListener("click", function (e) {
  fetch(requestPoints) // j'envoie ma requête
    .then(
      // quand je reçois la réponse
      (response) => response.json() // je récupère le JSON de la réponse
    )
    .then(
      // quand c'est pret
      (tab) => {
        monTab = tab;
        creatTabPoints(monTab);
        // je mets mes data dans ma variable
      }
    )
    .catch((err) => {
      console.log(err); // j'affiche les erreurs HTTP au cas où
    });
  area.innerHTML = "";
  key_Nm.innerHTML = "";
  area.style.gridTemplateColumns = "repeat(4, 1fr)";
});

let product_json = document.getElementById("product");
product_json.addEventListener("click", function (e) {
  fetch(requestProducts) // j'envoie ma requête
    .then(
      // quand je reçois la réponse
      (response) => response.json() // je récupère le JSON de la réponse
    )
    .then(
      // quand c'est pret
      (tab) => {
        monTab = tab;
        creatTabProducts(monTab);
        // je mets mes data dans ma variable
      }
    )
    .catch((err) => {
      console.log(err); // j'affiche les erreurs HTTP au cas où
    });
  area.innerHTML = "";
  key_Nm.innerHTML = "";
  area.style.gridTemplateColumns = "repeat(5, 1fr)";
});

function creatTabperson(monTab) {
  for (var key in monTab) {
    data = monTab[key];
    tabPerson = []
    var creatTxtPerson = document.createElement("div");
    var creatElementPerson = document.createElement("input");
    creatTxtPerson.classlist.add("rowCell");
    creatElementPerson.appendChild("creatTxtPerson");

    var tabPerson =  area.innerHTML += `
    <div class="rowCell">
        <input class="item_value input_id" type="text" class="textalign" data-id="${data.id}" data-attr="${data.id}" value="${data.id}">
        
        <div class="cell">
        <img src="${data.picture}" alt="image">
        </div>
        
        <input class="item_value input_id" type="text" class="textalign" data-id="${data.id}" data-attr="${data.age}" value="${data.age}">

        <input class="item_value inputClass" type="text" class="textalign" data-id="${data.id}" data-attr="${data.name}" value="${data.name}">

        <input class="item_value inputClass" type="text" class="textalign" data-id="${data.id}" data-attr="${data.gender}" value="${data.gender}">
        
        <input class="item_value inputClass" type="text" class="textalign" data-id="${data.id}" data-attr="${data.about}" value="${data.about}">

        <input class="item_value iinputClass type="text" class="textalign" data-id="${data.id}" data-attr="${data.registered}" value="${data.registered}">
        </div>
        `;
  }
  for (var key of Object.keys(data)) {
    key_Nm.innerHTML += `
        <p class="data-key center">${key}</p>
        `;
  }
  console.log(monTab)
  // monTab.forEach(element => {
 //   console.log(element)
 // });
 monTab.forEach(element => {
  let b = element.id;
  let c = element.name;
  let d = element.picture;
  let e = element.age;
  let newTabDeux = [b,c,d,e];
   console.log(newTabDeux);
});

}

function creatTabPoints(monTab) {
  for (var key in monTab) {
    data = monTab[key];
    var tabPoints = area.innerHTML += `
    <div class="rowCell">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.id}" value="${data.id}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.x}" value="${data.x}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.y}" value="${data.y}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.z}" value="${data.z}">
    </div>
    `;
  }
  for (var key of Object.keys(data)) {
    key_Nm.innerHTML += `
        <p class="data-key center">${key}</p>
        `;
  }
  console.log(monTab)
  // monTab.forEach(element => {
 //   console.log(element)
 // });
}

function creatTabProducts(monTab) {
  for (var key in monTab) {
    data = monTab[key];
    var tabProducts = area.innerHTML += `
    <div class="rowCell">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.id}" value="${data.id}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.price}" value="${data.price}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.name}" value="${data.name}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.available}" value="${data.available}">
    <input type="text" class="inputClass" data-id="${data.id}" data-attr="${data.tags}" value="${data.tags}">
    </div>`;
  }
  for (var key of Object.keys(data)) {
    key_Nm.innerHTML += `
        <p class="data-key center">${key}</p>
        `;
  }
  console.log(monTab)
  // monTab.forEach(element => {
 //   console.log(element)
 // });
}
let Arr = [requestPerson, requestPoints, requestProducts];
let test = [requestPerson]

/*
input_search.addEventListener("change", e => {
  const element = e.target.value.toLowerCase();
  const recherche = test.filter(monTab =>
    monTab.toLowerCase().includes(element));

  creatTabperson(recherche);
});


let inputClass = document.getElementsByClassName("inputClass")
input_search = document.getElementById("input_search") 
input_search.addEventListener("input", e => {
  for(val of inputClass){
    bcClass = inputClass.value;
    console.log(bcClass);
  for (element of bcClass) {
    search = input_search.value;

  }
}
});


input_search.addEventListener("input" , function(e){
  for(var key in monTab){
    data = monTab[key];
    if(data == input_search.value){
      
      data.style.display = "grid";
    }else{
      data.style.display = "none";
    }

  }
})
*/

input_search.addEventListener("input" , function(e){
    let element = e.target.value.toLowerCase()
    let newTab = monTab.filter(name => name.valuetoLowerCase().includes(element));
});




/*
var fruits = ['pomme', 'banane', 'raisin', 'mangue'];

function filtreTexte(arr, requete) {
  return arr.filter(function (el) {
    return el.toLowerCase().indexOf(requete.toLowerCase()) !== -1;
  })
}

console.log(filtreTexte(fruits, 'an')); // ['banane', 'mangue'];
console.log(filtreTexte(fruits, 'm')); // ['pomme', 'mangue'];
//boucler toute les les ligne 
//



inputClass.addEventListener("input", e => {
  const element = e.target.value.toLowerCase()
  const trie = monTab.filter(data => data.name.toLowerCase().include(element))
})
*/